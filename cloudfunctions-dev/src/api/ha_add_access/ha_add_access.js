import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import utils from '../../utils/utils.js';
import accessFunc from '../../utils/accessFunc.js';

const db = uniCloud.database();

async function Perform(event) {
	let result = await accessFunc.checkAccessSubmit(event);

	if (result.pass != true) {
		const {
			msg
		} = result;

		return {
			status: -1,
			msg
		}
	}

	let isPass = await accessFunc.checkAccessNumber(event.group_id);

	if (isPass != true) {
		return {
			status: -3,
			msg: '抱歉！出入点数量超限了。'
		};
	}

	// 检查模板是否存在
	const collection_tpl = db.collection('ha_template_list');
	let res_tpl = await collection_tpl
		.doc(event.tpl_id)
		.get();

	if (res_tpl.data && res_tpl.affectedDocs === 1){
	} else {
		return {
			status: -2,
			msg: "抱歉，模板不存在！"
		}
	}

	// 出入点信息
	delete event.uniCloudClientInfo;
	delete event.account_id;
	event.tpl_name = res_tpl.data[0].name;
	event.content = res_tpl.data[0].content;
	event.check_code = utils.random(10000000,99999999);

	let res_access = await db.collection('ha_access_list').add(event);

	return {
		status: 0,
		access_id: res_access.id,
		msg: "创建成功！"
	}
}

export {
	Perform as main
}
