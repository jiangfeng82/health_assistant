import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import accessFunc from '../../utils/accessFunc.js';

const db = uniCloud.database();
const collection = db.collection('ha_access_record');

async function Perform(event) {
	if (!event.access_id) {
		return {
			status: -1,
			msg: '出错了'
		}
	}

	let data = {
		access_id: event.access_id,
		access_state: 1,
		create_time: Date.now(),
		model: event.model
	}

	await collection.add(data);

	return {
		status: 0,
		msg: '提交成功！'
	}
}

export {
	Perform as main
}
