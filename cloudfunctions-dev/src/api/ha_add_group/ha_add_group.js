import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import groupFunc from '../../utils/groupFunc.js';

const db = uniCloud.database();

async function Perform(event) {
	if (JSON.stringify(event) === "{}") {
		return {
			code: -1,
			msg: "当前填写的信息为空！"
		};
	}

	if (!event.name) {
		return {
			code: -1,
			msg: "团体名不能为空！"
		};
	}

	if (!event.label) {
		return {
			code: -1,
			msg: "标签不能为空！"
		};
	}

	if (!event.account_id) {
		// -2=账号出现异常，重新登陆！
		return {
			code: -2,
			msg: "账号已过期，请重新登陆！"
		};
	}

	let isPass = await groupFunc.checkGroupNumber(event.account_id);

	if (isPass != true) {
		return {
			code: -3,
			msg: '抱歉！申请团体数量超限了。'
		};
	}

	const collection = db.collection('ha_group_list');
	let data = await collection.add(event);
	return {
		code: 0,
		data,
		msg: "团体创建成功！"
	};
}

export {
	Perform as main
}
