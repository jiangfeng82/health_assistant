import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import reportFunc from '../../utils/reportFunc.js';

const db = uniCloud.database();

async function Perform(event) {
	if (!event.report_id) {
		return {
			status: -1,
			msg: '出错了'
		}
	}

	if (!event.gp_id) {
		return {
			status: -1,
			msg: '出错了'
		}
	}

	let isPass = await reportFunc.checkReportRecord(event.report_id,event.gp_id);

	if (isPass != true) {
		return {
			status: -2,
			msg: '请不要重复提交！'
		}
	}

	let data = {
		report_id: event.report_id,
		gp_id: event.gp_id,
		create_time: Date.now(),
		allow_modify: 0,
		model: event.model
	}

	const collection = db.collection('ha_report_record');
	await collection.add(data);

	return {
		status: 0,
		msg: '提交成功！'
	}
}

export {
	Perform as main
}
