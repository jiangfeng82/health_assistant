import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();
const collection_p = db.collection('ha_group_person');
const collection1 = db.collection('ha_report_list');

async function Perform(event) {
	if (event.type === 0) {
		let user = await collection_p
			.doc(
				event.gp_id
			)
			.get();

		if (user.data && user.affectedDocs === 1){
		} else {
			return {
				status: -1,
				msg: "团体成员不存在！"
			}
		}

		let res1 = await collection1
			.doc(
				event.report_id
			)
			.get();

		if (res1.data && res1.affectedDocs === 1){
		} else {
			return {
				status: -2,
				msg: "抱歉，报备不存在！"
			}
		}

		if ( user.data[0].group_id != res1.data[0].group_id ) {
			return {
				status: -3,
				msg: "出错了！"
			}
		}
	} else if (event.type === 1) {
		let res1 = await collection1
			.doc(
				event.report_id
			)
			.get();

		if (res1.data && res1.affectedDocs === 1){
		} else {
			return {
				status: -2,
				msg: "抱歉，报备不存在！"
			}
		}

		if (!event.person_name) {
			return {
				status: -1,
				msg: '姓名有误！'
			}
		} else if (!event.person_identity ||
			!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/
			.test(event.person_identity)) {
			return {
				status: -1,
				msg: '身份证号有误，请重新输入！'
			}
		}

		let user = await collection_p
			.where({
				group_id: res1.data[0].group_id,
				// person_name: event.person_name.trim(),
				person_name: event.person_name,
				person_identity: event.person_identity
			})
			.get();

		if (user.data && user.affectedDocs >= 1){
		} else {
			return {
				status: -1,
				msg: "团体成员不存在！"
			}
		}
	} else {
		return {
			status: -10,
			msg: "出错了！"
		}
	}

	const {
		person_name,
		person_identity
	} = user.data[0];

	let data = {
		gp_id: user.data[0]._id,
		memberDetail: {
			person_name,
			person_identity
		},
		schema: res1.data[0].content
	};

	return {
		status: 0,
		data,
		msg: "获取成功"
	}
}

export {
	Perform as main
}
