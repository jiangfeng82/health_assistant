import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import validateToken from '../../utils/validateToken.js';
const db = uniCloud.database();

async function Perform(event) {
	if (!event.token) {
		return {
			status: -4,
			errCode: 'TOKEN_INVALID',
			msg: '出错了！'
		}
	}

	try {
		let result = await validateToken(event.token);
		if (result.status != 0) {
			return result;
		}
	} catch (e) {
		return {
			status: -2,
			errCode: 'TOKEN_INVALID',
			// msg: 'token无效'
			msg: '请重新登录'
		}
	}

	try {
		await db.collection('ha_access_record')
			.where({
				_id: event.record_id,
				access_id: event.access_id
			})
			.remove();
	} catch (e) {
		return {
			status: -10,
			msg: "删除失败！"
		}
	}

	return {
		status: 0,
		msg: "删除成功！"
	}
}

export {
	Perform as main
}
