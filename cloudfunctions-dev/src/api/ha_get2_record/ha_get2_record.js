import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import validateToken from '../../utils/validateToken.js';
import reportFunc from '../../utils/reportFunc.js';
import accessFunc from '../../utils/accessFunc.js';

async function Perform(event) {
	if (event.type != 2) {
		if (!event.token) {
			return {
				status: -4,
				errCode: 'TOKEN_INVALID',
				msg: '出错了！'
			}
		}
	
		try {
			let result = await validateToken(event.token);
			if (result.status != 0) {
				return result;
			}
		} catch (e) {
			return {
				status: -2,
				errCode: 'TOKEN_INVALID',
				// msg: 'token无效'
				msg: '请重新登录'
			}
		}
	}

	const page = Number(event.page) || 0;
    const length = Number(event.length) || 10;

	let data = [];
	if (event.type === 0) {
		// 报备记录
		data = await reportFunc.getReportRecord2(event.report_id,page,length);
	} else if ( (event.type === 1) || (event.type === 2) ) {
		// 出入点进出记录
		data = await accessFunc.getAccessRecord(event.access_id,page,length);
	}

	return {
		status: 0,
		data,
		msg: "获取成功！"
	}
}

export {
	Perform as main
}
