import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import reportFunc from '../../utils/reportFunc.js';
import accessFunc from '../../utils/accessFunc.js';

const db = uniCloud.database();
const dbCmd = db.command;

async function Perform(event) {
	let data = false;

	if (event.type === 0) { // 指定日期（一天）
		data = await reportFunc.getReportRecord1(event.report_id,{
			create_time: dbCmd.and(dbCmd.gt(event.create_time), dbCmd.lt(event.create_time + 86400000))
		});
	} else if (event.type === 1) { // 查询指定时间段的已报备情况
		if (/^[0-9]+$/.test(event.report_range)) {
			let now = Date.now();
			let before = now - 3600000 * event.report_range;
			data = await reportFunc.getReportRecord1(event.report_id,{
				create_time: dbCmd.gt(before)
			});
		}
	} else if (event.type === 2) { // 查询指定时间段的未报备情况
		if (/^[0-9]+$/.test(event.report_range)) {
			let now = Date.now();
			let before = now - 3600000 * event.report_range;
			data = await reportFunc.getReportUndo(event.report_id,{
				create_time: dbCmd.gt(before)
			});
		}
	} else if (event.type === 3) { // 查看报备
		data = await reportFunc.getOneRecord(event.record_id);
	} else if (event.type === 4) { // 查看出入登记
		data = await accessFunc.getOneRecord(event.record_id);
	}

	if (data == false) {
		return {
			status: -1,
			msg: "抱歉，没有找到相关记录！"
		}
	}

	return {
		status: 0,
		data,
		msg: "获取成功"
	}
}

export {
	Perform as main
}
