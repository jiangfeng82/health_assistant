import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import reportFunc from '../../utils/reportFunc.js';

const db = uniCloud.database();
const collection = db.collection('ha_report_list');

async function Perform(event) {
	if (event.type === 0) { // list
		let res = await collection
			.where({
				group_id: event.group_id
			})
			.get();

		return res.data;
	} else if (event.type === 1) { // detail
		let res = await collection
			.doc(
				event.report_id
			)
			.get();

		if (res.data && res.affectedDocs === 1) {
		} else {
			return {
				status: -1,
				msg: "抱歉，报备不存在！"
			}
		}

		// 报备记录
		let reportDone = await reportFunc.getReportRecord2(event.report_id,0,10);

		return {
			status: 0,
			data: {
				self: res.data[0],
				done: reportDone
			},
			msg: "获取成功"
		}
	} else if (event.type === 2) { // one
		let res = await collection
			.doc(
				event.report_id
			)
			.get();

		if (res.data && res.affectedDocs === 1) {
		} else {
			return {
				status: -1,
				msg: "抱歉，报备不存在！"
			}
		}

		return {
			status: 0,
			data: res.data[0],
			msg: "获取成功"
		}
	}
}

export {
	Perform as main
}
