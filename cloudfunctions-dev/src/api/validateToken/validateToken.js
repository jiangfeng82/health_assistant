import validateToken from '../../utils/validateToken.js'

const db = uniCloud.database()
async function Perform(event) {
	try {
		return await validateToken(event.token)
	} catch (e) {
		//TODO handle the exception
		return {
			status: -2,
			errCode: 'TOKEN_INVALID',
			// msg: 'token无效'
			msg: '请重新登录'
		}
	}
};

export {
	Perform as main
}
