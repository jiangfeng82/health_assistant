
function random(lower, upper) {
	return Math.floor(Math.random() * (upper - lower)) + lower;
}

// 去除字符串内所有的空格：str = str.replace(/\s*/g,"");
// 去除字符串内两头的空格：str = str.replace(/^\s*|\s*$/g,"");
// 去除字符串内左侧的空格：str = str.replace(/^\s*/,"");
// 去除字符串内右侧的空格：str = str.replace(/(\s*$)/g,"");

export default {
	random
}