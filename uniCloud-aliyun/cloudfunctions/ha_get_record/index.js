'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var crypto = _interopDefault(require('crypto'));

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var jwt_1 = createCommonjsModule(function (module) {
/*
 * jwt-simple
 *
 * JSON Web Token encode and decode module for node.js
 *
 * Copyright(c) 2011 Kazuhito Hokamura
 * MIT Licensed
 */

/**
 * module dependencies
 */



/**
 * support algorithm mapping
 */
var algorithmMap = {
  HS256: 'sha256',
  HS384: 'sha384',
  HS512: 'sha512',
  RS256: 'RSA-SHA256'
};

/**
 * Map algorithm to hmac or sign type, to determine which crypto function to use
 */
var typeMap = {
  HS256: 'hmac',
  HS384: 'hmac',
  HS512: 'hmac',
  RS256: 'sign'
};


/**
 * expose object
 */
var jwt = module.exports;


/**
 * version
 */
jwt.version = '0.5.6';

/**
 * Decode jwt
 *
 * @param {Object} token
 * @param {String} key
 * @param {Boolean} [noVerify]
 * @param {String} [algorithm]
 * @return {Object} payload
 * @api public
 */
jwt.decode = function jwt_decode(token, key, noVerify, algorithm) {
  // check token
  if (!token) {
    throw new Error('No token supplied');
  }
  // check segments
  var segments = token.split('.');
  if (segments.length !== 3) {
    throw new Error('Not enough or too many segments');
  }

  // All segment should be base64
  var headerSeg = segments[0];
  var payloadSeg = segments[1];
  var signatureSeg = segments[2];

  // base64 decode and parse JSON
  var header = JSON.parse(base64urlDecode(headerSeg));
  var payload = JSON.parse(base64urlDecode(payloadSeg));

  if (!noVerify) {
    if (!algorithm && /BEGIN( RSA)? PUBLIC KEY/.test(key.toString())) {
      algorithm = 'RS256';
    }

    var signingMethod = algorithmMap[algorithm || header.alg];
    var signingType = typeMap[algorithm || header.alg];
    if (!signingMethod || !signingType) {
      throw new Error('Algorithm not supported');
    }

    // verify signature. `sign` will return base64 string.
    var signingInput = [headerSeg, payloadSeg].join('.');
    if (!verify(signingInput, key, signingMethod, signingType, signatureSeg)) {
      throw new Error('Signature verification failed');
    }

    // Support for nbf and exp claims.
    // According to the RFC, they should be in seconds.
    if (payload.nbf && Date.now() < payload.nbf*1000) {
      throw new Error('Token not yet active');
    }

    if (payload.exp && Date.now() > payload.exp*1000) {
      throw new Error('Token expired');
    }
  }

  return payload;
};


/**
 * Encode jwt
 *
 * @param {Object} payload
 * @param {String} key
 * @param {String} algorithm
 * @param {Object} options
 * @return {String} token
 * @api public
 */
jwt.encode = function jwt_encode(payload, key, algorithm, options) {
  // Check key
  if (!key) {
    throw new Error('Require key');
  }

  // Check algorithm, default is HS256
  if (!algorithm) {
    algorithm = 'HS256';
  }

  var signingMethod = algorithmMap[algorithm];
  var signingType = typeMap[algorithm];
  if (!signingMethod || !signingType) {
    throw new Error('Algorithm not supported');
  }

  // header, typ is fixed value.
  var header = { typ: 'JWT', alg: algorithm };
  if (options && options.header) {
    assignProperties(header, options.header);
  }

  // create segments, all segments should be base64 string
  var segments = [];
  segments.push(base64urlEncode(JSON.stringify(header)));
  segments.push(base64urlEncode(JSON.stringify(payload)));
  segments.push(sign(segments.join('.'), key, signingMethod, signingType));

  return segments.join('.');
};

/**
 * private util functions
 */

function assignProperties(dest, source) {
  for (var attr in source) {
    if (source.hasOwnProperty(attr)) {
      dest[attr] = source[attr];
    }
  }
}

function verify(input, key, method, type, signature) {
  if(type === "hmac") {
    return (signature === sign(input, key, method, type));
  }
  else if(type == "sign") {
    return crypto.createVerify(method)
                 .update(input)
                 .verify(key, base64urlUnescape(signature), 'base64');
  }
  else {
    throw new Error('Algorithm type not recognized');
  }
}

function sign(input, key, method, type) {
  var base64str;
  if(type === "hmac") {
    base64str = crypto.createHmac(method, key).update(input).digest('base64');
  }
  else if(type == "sign") {
    base64str = crypto.createSign(method).update(input).sign(key, 'base64');
  }
  else {
    throw new Error('Algorithm type not recognized');
  }

  return base64urlEscape(base64str);
}

function base64urlDecode(str) {
  return Buffer.from(base64urlUnescape(str), 'base64').toString();
}

function base64urlUnescape(str) {
  str += new Array(5 - str.length % 4).join('=');
  return str.replace(/\-/g, '+').replace(/_/g, '/');
}

function base64urlEncode(str) {
  return base64urlEscape(Buffer.from(str).toString('base64'));
}

function base64urlEscape(str) {
  return str.replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '');
}
});

const accessMaxNumber = 5; // 一个团体下最多的出入口数

const db = uniCloud.database();
const collection1 = db.collection('ha_report_list');
const collection2 = db.collection('ha_report_record');
const collection3 = db.collection('ha_group_person');

async function checkReportRecord(report_id,gp_id) {
	let res1 = await collection1
		.doc(report_id)
		.get();

	if (res1.data && res1.affectedDocs === 1) ; else {
		return false;
	}

	let res2 = await collection2
		.where({
			report_id,
			gp_id
		})
		.orderBy("create_time", "desc")
		.limit(1)
		.get();

	if (res2.data && res2.affectedDocs >= 1) {
		if (res2.data[0].allow_modify === 1) {
			return true;
		}

		// 检查间隔时间
		if (res1.data[0].interval_hour > 0) {
			if (Date.now() >= (res2.data[0].create_time + 3600000 * res1.data[0].interval_hour)) { // 一小时=3600000
				return true;
			}
		}

		return false;
	}

	return true;
}

async function getOneRecord(record_id) {
	let res2 = await collection2
		.doc(record_id)
		.get();

	if (res2.data && res2.affectedDocs === 1) ; else {
		return false;
	}

	if (res2.data[0].hasOwnProperty('report_id') == false) {
		return false;
	}

	let res1 = await collection1
		.doc(res2.data[0].report_id)
		.get();

	if (res1.data && res1.affectedDocs === 1) ; else {
		return false;
	}

	return {
		schema: res1.data[0].content,
		model: res2.data[0].model
	}
}

async function getReportRecord1(report_id,condition = {}) {
	let res2 = await collection2
		.where(
			Object.assign({
				report_id
			},condition)
		)
		.orderBy("create_time", "asc")
		.get();

	if (res2.data && res2.affectedDocs >= 1) ; else {
		return false;
	}

	let recordList = [];
	for (var i = 0; i < res2.data.length; i++) {
		let res3 = await collection3
			.doc(
				res2.data[i].gp_id
			)
			.get();

		let person_name = "未知";
		let person_identity = "";
		if (res3.data.length) {
			person_name = res3.data[0].person_name;
			person_identity = res3.data[0].person_identity;
		}

		recordList.push(
			Object.assign({
				person_name,
				person_identity,
				create_time: res2.data[i].create_time
			},
			res2.data[i].model
			)
		);
	}

	return recordList;
}

async function getReportList(group_id) {
	let res = await collection1
		.where({
			group_id
		})
		.get();

	return res.data;
}

async function checkReportName(group_id,name) {
	let res = await collection1
		.where({
			group_id,
			name
		})
		.get();

	if (res.data && res.affectedDocs >= 1) {
		return false;
	} else {
		return true;
	}
}

async function getReportUndo(report_id,condition = {}) {
	let res1 = await collection1
		.doc(report_id)
		.get();

	if (res1.data && res1.affectedDocs === 1) ; else {
		return false;
	}

	let res3 = await collection3
		.where({
			group_id: res1.data[0].group_id
		})
		.get();

	let personUndo = [];
	for (var i = 0; i < res3.data.length; i++) {
		let res2 = await collection2
			.where(
				Object.assign({
					report_id,
					gp_id: res3.data[i]._id
				},condition)
			)
			.get();

		if (res2.data && res2.affectedDocs >= 1) ; else {
			personUndo.push({person_name: res3.data[i].person_name});
		}
	}

	return personUndo;
}

async function getReportRecord2(report_id,page,length) {
	const result = await collection2
		.where({
			report_id
		})
		.orderBy("create_time","desc")
		.skip(length * page)
		.limit(length)
		.get();

	const recordList = [];
	for (var i = 0; i < result.data.length; i++) {
		const {
			_id,
			gp_id,
			create_time,
			model
		} = result.data[i];

		let tmp = await collection3
			.doc(gp_id)
			.get();

		let person_name = "未知";
		if (tmp.data.length) {
			person_name = tmp.data[0].person_name;
		}

		recordList.push({
			_id,
			gp_id,
			person_name,
			create_time,
			model
		});
	}

	return recordList;
}

var reportFunc = {
	checkReportRecord,
	getOneRecord,
	getReportRecord1,
	getReportList,
	checkReportName,
	getReportUndo,
	getReportRecord2
};

const db$1 = uniCloud.database();
const collection1$1 = db$1.collection('ha_access_list');
const collection2$1 = db$1.collection('ha_access_record');
const collection_user = db$1.collection('ha_user_account');
const collection_group = db$1.collection('ha_group_list');

// 检查提交的信息
async function checkAccessSubmit(submit) {
	let {
		name,
		account_id,
		group_id
	} = submit;

	name = name.replace(/\s*/g,"");
	if (name.length < 2) {
		return {
			pass: false,
			msg: '请输入出入点名称！'
		}
	}

	if (!account_id) {
		return {
			pass: false,
			msg: '请重新登录！'
		}
	}

	if (!group_id) {
		return {
			pass: false,
			msg: '请先进入团体，再创建！'
		}
	}

	// 检查用户
	let user = await collection_user
		.doc(account_id)
		.get();

	if (user.data && user.affectedDocs === 1); else {
		return {
			pass: false,
			msg: '请重新登录！'
		}
	}

	// 检查团体
	let res_group = await collection_group
		.doc(group_id)
		.get();

	if (res_group.data && res_group.affectedDocs === 1); else {
		return {
			pass: false,
			msg: '请先进入团体，再创建！'
		}
	}

	// 重名检查
	let res1 = await collection1$1
		.where({
			group_id,
			name
		})
		.get();

	if (res1.data && res1.affectedDocs >= 1) {
		return {
			pass: false,
			msg: '请不要创建重名出入点！'
		}
	}

	return {
		pass: true
	}
}

async function updateAccessItem(access_id,access_info) {
	await collection1$1
		.doc(access_id)
		.update(access_info);
}

async function checkAccessNumber(group_id) {
	let res = await collection1$1
		.where({
			group_id
		})
		.get();

	if (res.data && res.affectedDocs >= accessMaxNumber) {
		return false
	}
	return true;
}

async function getAccessRecord(access_id,page,length,condition = {}) {
	const result = await collection2$1
		.where(
			Object.assign({
				access_id
			},condition)
		)
		.orderBy("create_time","desc")
		.skip(length * page)
		.limit(length)
		.get();

	const recordList = [];
	for (var i = 0; i < result.data.length; i++) {
		const {
			_id,
			access_state,
			create_time,
			model
		} = result.data[i];

		recordList.push({
			_id,
			access_state,
			create_time,
			model
		});
	}

	return recordList;
}

async function getOneRecord$1(record_id) {
	let res2 = await collection2$1
		.doc(record_id)
		.get();

	if (res2.data && res2.affectedDocs === 1) ; else {
		return false;
	}

	if (res2.data[0].hasOwnProperty('access_id') == false) {
		return false;
	}

	let res1 = await collection1$1
		.doc(res2.data[0].access_id)
		.get();

	if (res1.data && res1.affectedDocs === 1) ; else {
		return false;
	}

	return {
		schema: res1.data[0].content,
		model: res2.data[0].model
	}
}

async function checkAccessItem(access_id,condition) {
	let res1 = await collection1$1
		.where(
			Object.assign({
				_id: access_id
			},condition)
		)
		.get();

	if (res1.data && res1.affectedDocs >= 1) {
		return true;
	} else {
		return false;
	}
}

var accessFunc = {
	checkAccessSubmit,
	updateAccessItem,
	checkAccessNumber,
	getAccessRecord,
	getOneRecord: getOneRecord$1,
	checkAccessItem
};

const db$2 = uniCloud.database();
const dbCmd = db$2.command;

async function Perform(event) {
	let data = false;

	if (event.type === 0) { // 指定日期（一天）
		data = await reportFunc.getReportRecord1(event.report_id,{
			create_time: dbCmd.and(dbCmd.gt(event.create_time), dbCmd.lt(event.create_time + 86400000))
		});
	} else if (event.type === 1) { // 查询指定时间段的已报备情况
		if (/^[0-9]+$/.test(event.report_range)) {
			let now = Date.now();
			let before = now - 3600000 * event.report_range;
			data = await reportFunc.getReportRecord1(event.report_id,{
				create_time: dbCmd.gt(before)
			});
		}
	} else if (event.type === 2) { // 查询指定时间段的未报备情况
		if (/^[0-9]+$/.test(event.report_range)) {
			let now = Date.now();
			let before = now - 3600000 * event.report_range;
			data = await reportFunc.getReportUndo(event.report_id,{
				create_time: dbCmd.gt(before)
			});
		}
	} else if (event.type === 3) { // 查看报备
		data = await reportFunc.getOneRecord(event.record_id);
	} else if (event.type === 4) { // 查看出入登记
		data = await accessFunc.getOneRecord(event.record_id);
	}

	if (data == false) {
		return {
			status: -1,
			msg: "抱歉，没有找到相关记录！"
		}
	}

	return {
		status: 0,
		data,
		msg: "获取成功"
	}
}

exports.main = Perform;
