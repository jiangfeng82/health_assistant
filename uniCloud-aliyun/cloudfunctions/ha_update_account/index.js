"use strict";
exports.main = async (event, context) => {
	//event为客户端上传的参数
	// console.log('event : ' + event)
	// //返回数据给客户端
	// return event

	//   ha_user_account
	// {
	//     _id: "", // string，自生成
	//     username: "", // string 用户
	//     password: "", // string 密码(禁止明文)
	//     mobile: "", // string 手机号，需验证符合规则
	//     email: "", // string 邮箱
	// }
	const db = uniCloud.database();
	if (JSON.stringify(event) === "{}") {
		return {
			code: -1,
			msg: "当前填写的信息不能为空！"
		};
	} else {
		let account_id = event.account_id;
		delete event.account_id

		if (!event.mobile || !(/^1[3456789]\d{9}$/.test(event.mobile))) {
			return {
				code: -1,
				msg: "手机号码有误，请重填！"
			};
		} else if (!event.email || !(/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(event.email))) {
			return {
				code: -1,
				msg: "邮箱填写有误，请重填！"
			};
		} else {
			const data = await db
				.collection("ha_user_account")
				.doc(account_id)
				.update(event);

			if (data.updated) {
				return {
					code: 0,
					msg: '更新成功！'
				}
			} else {
				return {
					code: -1,
					msg: '更新失败，请稍后重试！'
				}
			}

		}
	}
};
